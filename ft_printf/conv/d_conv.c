/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   d_conv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 16:41:27 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 14:31:46 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

void		cast_di(va_list *params, t_var *var, t_conv *conv)
{
	if (var->lm.hh)
		conv->str = ft_sitoa((char)va_arg(*params, int));
	else if (var->lm.h)
		conv->str = ft_sitoa((short)va_arg(*params, int));
	else if (var->lm.l)
		conv->str = ft_sitoa((long)va_arg(*params, long));
	else if (var->lm.ll)
		conv->str = ft_sitoa((long long)va_arg(*params, long long));
	else
		conv->str = ft_sitoa((int)va_arg(*params, int));
}

void		cast_ux(va_list *params, t_var *var, t_conv *conv)
{
	if (var->lm.hh)
		conv->str = ft_uitoa((unsigned char)va_arg(*params, unsigned int));
	else if (var->lm.h)
		conv->str = ft_uitoa((unsigned short)va_arg(*params, unsigned int));
	else if (var->lm.l)
		conv->str = ft_uitoa((unsigned long)va_arg(*params, unsigned long));
	else if (var->lm.ll)
		conv->str = ft_uitoa((unsigned long long)
							va_arg(*params, unsigned long long));
	else
		conv->str = ft_uitoa((unsigned int)va_arg(*params, unsigned int));
}

void		d_is_null(t_conv *conv)
{
	if (conv->str[0] == '0')
	{
		free(conv->str);
		conv->b_empty_s = true;
		conv->str = (unsigned char *)"0";
	}
}

int			d_conv(va_list *params, t_var *var, t_conv *conv)
{
	cast_di(params, var, conv);
	if (ft_str_isnegnum(conv->str))
		conv->b_neg_num = true;
	d_is_null(conv);
	conv->lenstr = ft_strlen((const char *)conv->str);
	if (conv->b_empty_s && var->flags.b_precision && var->pr == 0)
		conv->lenstr = 0;
	if (var->flags.b_precision && !var->pr && conv->b_empty_s)
	{
		write_fw(var, conv);
		return (conv->lenstr);
	}
	if (var->flags.b_precision && var->pr)
		var->flags.b_zero = true;
	call_flags(var, conv);
	if (var->flags.b_field_width)
		write_fw(var, conv);
	if (!var->flags.b_minus)
		conv->length += write(STD, conv->str, conv->lenstr);
	if (!conv->b_empty_s)
		free(conv->str);
	return (conv->lenstr);
}
