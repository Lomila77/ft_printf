/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hexa.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 16:39:48 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 17:11:45 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

void		len_hexa(unsigned long long conv_p, t_conv *conv)
{
	if (conv_p < 16)
		conv->lenstr++;
	if (conv_p >= 16)
	{
		len_hexa(conv_p / 16, conv);
		conv->lenstr++;
	}
}

void		display_hexa(unsigned long long conv_p, t_conv *conv)
{
	static char *base = "0123456789abcdef";

	if (conv->b_empty_s)
	{
		conv->length += write(STD, conv->str, conv->lenstr);
		return ;
	}
	if (conv_p < 16)
		conv->length += write(STD, &base[conv_p % 16], 1);
	if (conv_p >= 16)
	{
		display_hexa(conv_p / 16, conv);
		conv->length += write(STD, &base[conv_p % 16], 1);
	}
}

void		len_hexa_xmaj(unsigned long long conv_p, t_conv *conv)
{
	if (conv_p < 16)
		conv->lenstr++;
	if (conv_p >= 16)
	{
		len_hexa(conv_p / 16, conv);
		conv->lenstr++;
	}
}

void		display_hexa_xmaj(unsigned long long conv_p, t_conv *conv)
{
	static char *base = "0123456789ABCDEF";

	if (conv->b_empty_s)
	{
		conv->length += write(STD, conv->str, conv->lenstr);
		return ;
	}
	if (conv_p < 16)
		conv->length += write(STD, &base[conv_p % 16], 1);
	if (conv_p >= 16)
	{
		display_hexa_xmaj(conv_p / 16, conv);
		conv->length += write(STD, &base[conv_p % 16], 1);
	}
}
