/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_conv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 16:41:17 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 16:41:18 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flags.h"
#include "conv.h"

void	init_conv(t_conv *conv)
{
	conv->indic = 0;
	conv->str = NULL;
	conv->b_empty_s = false;
	conv->b_neg_num = false;
	conv->length = 0;
	conv->lenstr = 0;
}
