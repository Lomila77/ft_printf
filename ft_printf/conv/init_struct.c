/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_struct.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 16:41:03 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 16:41:06 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

void	init_struct(t_conv *content)
{
	content[0] = (t_conv){.indic = '%', .f = &pourcent_conv};
	content[1] = (t_conv){.indic = 'c', .f = &c_conv};
	content[2] = (t_conv){.indic = 's', .f = &s_conv};
	content[3] = (t_conv){.indic = 'd', .f = &d_conv};
	content[4] = (t_conv){.indic = 'i', .f = &d_conv};
	content[5] = (t_conv){.indic = 'u', .f = &u_conv};
	content[6] = (t_conv){.indic = 'x', .f = &x_conv};
	content[7] = (t_conv){.indic = 'X', .f = &xmaj_conv};
	content[8] = (t_conv){.indic = 'p', .f = &p_conv};
	content[10] = (t_conv){.indic = 'n', .f = &n_conv};
}
