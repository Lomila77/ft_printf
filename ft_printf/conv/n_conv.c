/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   n_conv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 16:40:04 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 16:40:52 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

int		n_conv(va_list *params, t_var *var, t_conv *conv)
{
	int *ptr;

	(void)conv;
	ptr = va_arg(*params, int *);
	*ptr = var->nbr_o;
	return (0);
}
