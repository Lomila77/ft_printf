/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   p_conv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 16:39:27 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 16:39:30 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

void		precision_and_flags(t_var *var, t_conv *conv)
{
	if (var->flags.b_precision)
	{
		if (var->pr > conv->lenstr)
			var->flags.b_zero = true;
		if (var->pr > conv->lenstr && var->pr > var->fw)
		{
			var->fw = var->pr + 2;
			var->pr = 0;
			var->flags.b_precision = false;
		}
		var->flags.b_field_width = true;
		var->flags.b_minus = false;
	}
	var->flags.b_plus = false;
	var->flags.b_space = false;
	var->flags.b_sharp = true;
}

void		print_if_minus_is_false(t_var *var, t_conv *conv)
{
	if (!var->flags.b_minus)
	{
		if (!var->flags.b_zero && !conv->b_empty_s)
			conv->length += write(STD, "0x", 2);
		if (!var->flags.b_zero && conv->b_empty_s)
			conv->length += write(STD, conv->str, conv->lenstr);
		else if (!conv->b_empty_s)
			display_hexa((unsigned long long)conv->p, conv);
	}
}

int			p_conv(va_list *params, t_var *var, t_conv *conv)
{
	conv->p = (unsigned long long)va_arg(*params, void *);
	conv->str = (unsigned char *)ft_uitoa(conv->p);
	if ((unsigned long long)conv->p == 0)
	{
		free(conv->str);
		conv->b_empty_s = true;
		if (var->flags.b_precision)
			conv->str = (unsigned char *)"0x";
		else
			conv->str = (unsigned char *)"0x0";
		conv->lenstr = ft_strlen((const char *)conv->str);
	}
	else
		len_hexa((unsigned long long)conv->p, conv);
	precision_and_flags(var, conv);
	call_flags(var, conv);
	write_fw(var, conv);
	print_if_minus_is_false(var, conv);
	if (!conv->b_empty_s)
		free(conv->str);
	return (conv->lenstr);
}
