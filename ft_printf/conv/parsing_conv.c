/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_conv.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 16:39:15 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 16:39:17 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

int		parsing_conv(const char *str, t_var *var, va_list *params)
{
	t_conv	conv[11];
	int		j;
	int		i;

	init_conv(conv);
	init_struct(conv);
	j = 0;
	i = 0;
	while (str[i] != conv[j].indic)
	{
		if (j >= 10)
		{
			i++;
			j = 0;
			if (str[i] == '\0')
				return (0);
		}
		j++;
	}
	conv[j].f(params, var, &conv[j]);
	var->nbr_o += conv[j].length;
	return (i + 1);
}
