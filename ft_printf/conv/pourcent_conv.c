/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pourcent_conv.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/03 10:51:18 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 16:38:59 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

int		pourcent_conv(va_list *params, t_var *var, t_conv *conv)
{
	unsigned char	pourcent;

	(void)params;
	pourcent = '%';
	conv->str = &pourcent;
	var->pr = false;
	var->flags.b_plus = false;
	var->flags.b_space = false;
	call_flags(var, conv);
	if (var->flags.b_field_width)
		write_fw(var, conv);
	if (!var->flags.b_minus)
		conv->length += write(STD, &pourcent, 1);
	return (1);
}
