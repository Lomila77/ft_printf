/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_conv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 16:38:44 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 10:48:09 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

int		s_conv(va_list *params, t_var *var, t_conv *conv)
{
	conv->str = (unsigned char *)va_arg(*params, char *);
	if (!conv->str)
	{
		conv->b_empty_s = true;
		conv->str = (unsigned char *)"(null)";
	}
	conv->lenstr = ft_strlen((const char *)conv->str);
	if (var->flags.b_precision && var->pr < conv->lenstr)
	{
		conv->lenstr = var->pr;
		var->flags.b_precision = false;
	}
	else if (var->flags.b_precision && var->pr > conv->lenstr)
		var->pr = 0;
	var->flags.b_plus = false;
	var->flags.b_sharp = false;
	var->flags.b_space = false;
	call_flags(var, conv);
	if (var->flags.b_field_width)
		write_fw(var, conv);
	if (!var->flags.b_minus)
		conv->length += write(STD, conv->str, conv->lenstr);
	return (conv->lenstr);
}
