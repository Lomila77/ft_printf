/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   u_conv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/03 10:35:58 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 16:47:03 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

void	u_is_null_and_flags(t_conv *conv, t_var *var)
{
	if (conv->str[0] == '0')
	{
		free(conv->str);
		conv->b_empty_s = true;
		conv->str = (unsigned char *)"0";
	}
	var->flags.b_plus = false;
	var->flags.b_space = false;
}

int		u_conv(va_list *params, t_var *var, t_conv *conv)
{
	cast_ux(params, var, conv);
	conv->lenstr = ft_strlen((const char *)conv->str);
	u_is_null_and_flags(conv, var);
	if (conv->b_empty_s && var->flags.b_precision && var->pr == 0)
		conv->lenstr = 0;
	if (var->flags.b_precision && !var->pr && conv->b_empty_s)
	{
		write_fw(var, conv);
		return (conv->lenstr);
	}
	if (var->flags.b_precision && var->pr)
		var->flags.b_zero = true;
	call_flags(var, conv);
	if (var->flags.b_field_width)
		write_fw(var, conv);
	if (!var->flags.b_minus)
		conv->length += write(STD, conv->str, conv->lenstr);
	if (!conv->b_empty_s)
		free(conv->str);
	return (conv->lenstr);
}
