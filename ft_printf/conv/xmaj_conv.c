/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   xmaj_conv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 17:01:16 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 17:05:09 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

void		xmaj_print_if_minus_is_false(t_conv *conv, t_var *var)
{
	if (!var->flags.b_minus)
	{
		if (!var->flags.b_zero && !conv->b_empty_s && var->flags.b_sharp)
			conv->length += write(STD, "0X", 2);
		if (conv->b_empty_s)
		{
			if ((var->flags.b_precision && var->pr != 0) ||
					!var->flags.b_precision)
				conv->length += write(STD, "0", 1);
		}
		else if (!conv->b_empty_s)
			display_hexa_xmaj((unsigned long long)conv->p, conv);
	}
}

void		xmaj_precision_is_true(t_conv *conv, t_var *var)
{
	if (var->flags.b_precision)
	{
		if (var->pr > conv->lenstr)
		{
			if (var->pr > var->fw)
				var->fw = var->pr;
			var->flags.b_zero = true;
		}
		if (var->flags.b_sharp)
			if (var->flags.b_zero && var->flags.b_precision)
				var->flags.b_minus = false;
		var->flags.b_field_width = true;
	}
}

void		xmaj_len_hexa_and_flags(t_conv *conv, t_var *var)
{
	if (conv->str[0] == '0')
	{
		free(conv->str);
		if (var->flags.b_sharp == true)
		{
			conv->str = (unsigned char *)"0X";
			conv->lenstr = ft_strlen((const char *)conv->str);
		}
		else
			conv->str = (unsigned char *)"0";
		conv->b_empty_s = true;
		conv->lenstr = 1;
		if (var->flags.b_precision && var->pr == 0)
			conv->lenstr = 0;
	}
	else
		len_hexa_xmaj((unsigned long long)conv->p, conv);
	var->flags.b_plus = false;
	var->flags.b_space = false;
}

int			xmaj_conv(va_list *params, t_var *var, t_conv *conv)
{
	cast_ux(params, var, conv);
	conv->p = (unsigned long long)ft_atoiu((const char *)conv->str);
	xmaj_len_hexa_and_flags(conv, var);
	xmaj_precision_is_true(conv, var);
	call_flags(var, conv);
	write_fw(var, conv);
	xmaj_print_if_minus_is_false(conv, var);
	if (!conv->b_empty_s)
		free(conv->str);
	return (conv->lenstr);
}
