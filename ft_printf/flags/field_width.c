/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   field_width.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 17:12:51 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 10:58:15 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flags.h"
#include "conv.h"
#include "libft.h"

void	width_is_star(t_var *var, int *cursor)
{
	if (var->fw < 0)
	{
		var->flags.b_minus = true;
		var->fw *= -1;
		if (var->flags.b_zero == true)
			var->flags.b_zero = false;
	}
	var->flags.b_field_width = true;
	(void)cursor;
}

int		field_width(const char *str, t_var *var, int *cursor)
{
	int i;

	i = 0;
	if (str[i] == '*')
	{
		width_is_star(var, cursor);
		*cursor += 1;
		return (i);
	}
	while (ft_isnum(str[i]))
		i++;
	if (str[i] == '\0')
		return (-1);
	var->fw = ft_batoi(str, i);
	if (i)
		var->flags.b_field_width = true;
	*cursor += i;
	return (i);
}

void	write_fw(t_var *var, t_conv *conv)
{
	if (conv->indic != 'n' && var->flags.b_field_width)
	{
		if (conv->indic == 'c' || conv->indic == '%')
		{
			while (var->fw-- > 1)
				conv->length += write(STD, " ", 1);
		}
		else if (conv->indic == 'p' || conv->indic == 'X' || conv->indic == 'x')
		{
			while (var->fw > conv->lenstr && var->fw > var->pr)
			{
				conv->length += write(STD, " ", 1);
				var->fw--;
			}
		}
		else
			while (var->fw > conv->lenstr && var->fw > var->pr)
			{
				conv->length += write(STD, " ", 1);
				var->fw--;
			}
		var->flags.b_field_width = false;
	}
}
