/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_flags.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 17:18:34 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 15:24:36 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flags.h"
#include "conv.h"

void	all_is_false(t_var *var)
{
	var->pr = 0;
	var->fw = 0;
	var->flags.b_sharp = false;
	var->flags.b_minus = false;
	var->flags.b_zero = false;
	var->flags.b_plus = false;
	var->flags.b_space = false;
	var->flags.b_precision = false;
	var->flags.b_field_width = false;
}

int		init_flags(const char *str, t_var *var)
{
	int i;

	i = 0;
	all_is_false(var);
	while (true)
	{
		if (str[i] == '#')
			var->flags.b_sharp = true;
		else if (str[i] == '-')
			var->flags.b_minus = true;
		else if (str[i] == '0')
			var->flags.b_zero = true;
		else if (str[i] == '+')
			var->flags.b_plus = true;
		else if (str[i] == ' ')
			var->flags.b_space = true;
		else
			break ;
		i++;
	}
	if (var->flags.b_minus == true)
		var->flags.b_zero = false;
	if (var->flags.b_plus == true)
		var->flags.b_space = false;
	return (i);
}

void	init_all(t_var *var, t_cur *cur)
{
	cur->new = 0;
	cur->old = 0;
	var->nbr_o = 0;
	var->pr = 0;
	var->fw = 0;
	var->flags.b_sharp = false;
	var->flags.b_minus = false;
	var->flags.b_zero = false;
	var->flags.b_plus = false;
	var->flags.b_space = false;
	var->flags.b_precision = false;
	var->flags.b_field_width = false;
}

int		call_flags(t_var *var, t_conv *conv)
{
	if (var->flags.b_sharp || var->flags.b_minus || var->flags.b_zero ||
		var->flags.b_plus || var->flags.b_space)
	{
		if (var->flags.b_sharp)
			sharp(var, conv);
		if (var->flags.b_minus)
			minus(var, conv);
		if (var->flags.b_plus)
			plus(var, conv);
		if (var->flags.b_space)
			space(var, conv);
		if (var->flags.b_zero)
			zero(var, conv);
		return (1);
	}
	return (0);
}
