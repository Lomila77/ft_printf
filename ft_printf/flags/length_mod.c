/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   length_mod.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 17:20:03 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 17:22:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flags.h"
#include "conv.h"
#include "libft.h"

void	length_is_false(t_var *var)
{
	var->lm.hh = false;
	var->lm.h = false;
	var->lm.ll = false;
	var->lm.l = false;
}

void	parse_lengthmod_plus_plus(t_var *var)
{
	if (var->lm.h >= 2)
		var->lm.hh = true;
	if (var->lm.l >= 2)
		var->lm.ll = true;
	if (var->lm.hh)
		var->lm.h = false;
	if (var->lm.ll)
		var->lm.l = false;
}

int		length_mod(const char *str, t_var *var)
{
	int i;

	i = 0;
	length_is_false(var);
	if (str[i] == 'h')
		while (str[i] == 'h')
		{
			var->lm.h++;
			i++;
		}
	else if (str[i] == 'l')
		while (str[i] == 'l')
		{
			var->lm.l++;
			i++;
		}
	if (str[i] == '\0')
		return (-1);
	if (!i)
		return (0);
	parse_lengthmod_plus_plus(var);
	return (i);
}
