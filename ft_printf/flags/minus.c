/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minus.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 17:23:09 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 17:24:05 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

void		minus_d(t_var *var, t_conv *conv)
{
	if ((conv->indic == 'd' || conv->indic == 'i') && var->pr < var->fw)
		while (var->pr > conv->lenstr)
		{
			conv->length += write(STD, "0", 1);
			var->pr--;
			var->fw--;
		}
}

void		minus_hexa(t_var *var, t_conv *conv)
{
	if (conv->indic == 'p' || conv->indic == 'x' || conv->indic == 'X')
	{
		if ((conv->indic == 'p' || conv->indic == 'x') && !conv->b_empty_s)
		{
			if (var->flags.b_sharp || conv->indic == 'p')
				conv->length += write(STD, "0x", 2);
		}
		else if (conv->indic == 'X' && !conv->b_empty_s && var->flags.b_sharp)
			if (!var->flags.b_zero)
				conv->length += write(STD, "0X", 2);
		if (conv->indic == 'X' && !conv->b_empty_s)
			display_hexa_xmaj((unsigned long long)conv->p, conv);
		else if (conv->indic == 'X' && conv->b_empty_s)
		{
			if ((var->flags.b_precision && var->pr != 0) ||
					!var->flags.b_precision)
				conv->length += write(STD, "0", 1);
		}
		else
			display_hexa((unsigned long long)conv->p, conv);
	}
}

void		minus_c(t_var *var, t_conv *conv)
{
	(void)var;
	if (conv->indic == 'c' || conv->indic == '%')
		conv->length += write(STD, &conv->str[0], 1);
}

void		minus(t_var *var, t_conv *conv)
{
	if (var->flags.b_space)
		space(var, conv);
	if (var->flags.b_plus)
		plus(var, conv);
	if (var->flags.b_zero)
		zero(var, conv);
	minus_c(var, conv);
	minus_hexa(var, conv);
	if (var->flags.b_minus)
		var->flags.b_zero = false;
	minus_d(var, conv);
	if (conv->indic != 'p' && conv->indic != 'X' && conv->indic != 'x')
		conv->length += write(STD, conv->str, conv->lenstr);
	if (var->flags.b_field_width)
		write_fw(var, conv);
}
