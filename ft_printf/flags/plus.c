/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   plus.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 17:24:26 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/04 17:30:09 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flags.h"
#include "conv.h"
#include "libft.h"

void	plus_d(t_var *var, t_conv *conv)
{
	if (conv->indic == 'd' || conv->indic == 'i')
	{
		if (var->flags.b_precision)
		{
			if (var->pr > conv->lenstr)
				var->fw--;
			if (!var->flags.b_minus)
				write_fw(var, conv);
			if (!conv->b_neg_num)
				conv->length += write(STD, "+", 1);
			else
				conv->length += write(STD, "-", 1);
			if (!var->flags.b_minus)
				zero(var, conv);
			var->flags.b_plus = false;
		}
		if (conv->b_neg_num && var->flags.b_zero)
			zero(var, conv);
	}
}

void	plus_print_field_width_and_sign(t_conv *conv, t_var *var)
{
	if ((conv->b_empty_s || !conv->b_neg_num) && conv->indic != 'p')
	{
		if (var->flags.b_precision || var->flags.b_zero)
		{
			conv->length += write(STD, "+", 1);
			if (!var->flags.b_precision)
				var->fw--;
			zero(var, conv);
		}
		else if (!var->flags.b_zero)
		{
			var->fw--;
			if (!var->flags.b_minus)
				write_fw(var, conv);
			conv->length += write(STD, "+", 1);
		}
	}
	else if (conv->b_neg_num && conv->indic != 'p')
	{
		var->fw--;
		if (!var->flags.b_minus)
			write_fw(var, conv);
	}
	else if (!var->flags.b_minus)
		write_fw(var, conv);
}

void	plus(t_var *var, t_conv *conv)
{
	plus_d(var, conv);
	if (!var->flags.b_plus)
		return ;
	plus_print_field_width_and_sign(conv, var);
	var->flags.b_plus = false;
}
