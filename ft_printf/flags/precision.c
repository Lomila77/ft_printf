/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   precision.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 09:23:48 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 10:59:17 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flags.h"
#include "conv.h"

void	precision_is_star(va_list *params, const char *str, t_var *var)
{
	if (str[0] == '.' && str[1] == '*')
	{
		var->flags.b_precision = true;
		var->pr = va_arg(*params, int);
	}
}

int		precision_is_true(int *cursor, t_var *var)
{
	if (var->flags.b_precision)
	{
		*cursor += 2;
		if (var->pr < 0)
		{
			var->flags.b_precision = false;
			var->pr = 0;
		}
		return (0);
	}
	return (1);
}

int		length_precision(const char *str, t_var *var, int *cursor,
						va_list *params)
{
	int i;

	i = 0;
	precision_is_star(params, str, var);
	if (!precision_is_true(cursor, var))
		return (0);
	if (str[i] == '.')
	{
		i++;
		while (ft_isnum(str[i]))
			i++;
	}
	else
		return (0);
	if (str[i] == '\0')
		return (-1);
	if (i > 1)
		var->pr = ft_batoi(&str[1], i - 1);
	var->flags.b_precision = true;
	*cursor += i;
	return (i);
}
