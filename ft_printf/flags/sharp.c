/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sharp.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/04 21:20:07 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 09:24:40 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

void	sharp(t_var *var, t_conv *conv)
{
	if (conv->indic == 'X' || conv->indic == 'x' || conv->indic == 'p')
	{
		if (var->flags.b_field_width && !var->flags.b_precision
			&& conv->indic != 'p')
			conv->lenstr += 2;
		else if (conv->indic == 'p' && !conv->b_empty_s)
			conv->lenstr += 2;
	}
}
