/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   space.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 09:26:01 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 09:29:19 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "conv.h"
#include "flags.h"
#include "libft.h"

void	space_d(t_var *var, t_conv *conv)
{
	if (conv->indic == 'd' || conv->indic == 'i')
	{
		if (var->flags.b_precision)
		{
			if (var->pr > conv->lenstr)
				var->fw--;
			if (!var->flags.b_minus)
				write_fw(var, conv);
			if (!conv->b_neg_num)
				conv->length += write(STD, " ", 1);
			else
				conv->length += write(STD, "-", 1);
			if (!var->flags.b_minus)
				zero(var, conv);
			var->flags.b_space = false;
		}
	}
}

void	space_hexa(t_var *var, t_conv *conv)
{
	if (conv->indic == 'p' || conv->indic == 'x' || conv->indic == 'X')
	{
		if (var->flags.b_field_width)
		{
			conv->length += write(STD, " ", 1);
			conv->lenstr++;
			if (var->flags.b_zero)
				write_fw(var, conv);
		}
		var->flags.b_space = false;
	}
}

void	space_empty_s_and_positiv_num(t_var *var, t_conv *conv)
{
	if ((!conv->b_empty_s && !conv->b_neg_num))
	{
		if (var->flags.b_precision || var->flags.b_zero)
		{
			conv->length += write(STD, " ", 1);
			if (!var->flags.b_precision)
				var->fw--;
			zero(var, conv);
		}
		else if (!var->flags.b_zero)
		{
			var->fw--;
			if (!var->flags.b_minus)
				write_fw(var, conv);
			conv->length += write(STD, " ", 1);
		}
	}
}

void	space(t_var *var, t_conv *conv)
{
	space_d(var, conv);
	if (!var->flags.b_space)
		return ;
	space_empty_s_and_positiv_num(var, conv);
	if (conv->b_neg_num)
	{
		var->fw--;
		if (!var->flags.b_minus)
			write_fw(var, conv);
	}
	else if (!var->flags.b_minus)
		write_fw(var, conv);
	space_hexa(var, conv);
	var->flags.b_space = false;
}
