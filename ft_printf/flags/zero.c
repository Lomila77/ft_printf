/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zero.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 09:30:07 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 15:13:41 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flags.h"
#include "conv.h"
#include "libft.h"

void	zero_c(t_var *var, t_conv *conv)
{
	int i;

	i = 0;
	if (conv->indic == 'c' || conv->indic == '%')
	{
		if (var->flags.b_field_width)
			while (i < var->fw - 1)
			{
				conv->length += write(STD, "0", 1);
				i++;
			}
		var->flags.b_field_width = false;
	}
}

int		zero_hexa(t_var *var, t_conv *conv)
{
	if (conv->indic == 'p' || conv->indic == 'x' || conv->indic == 'X')
	{
		if (var->flags.b_field_width && var->flags.b_precision)
		{
			if (var->fw > var->pr && !var->flags.b_minus)
				write_fw(var, conv);
			if (conv->indic == 'X' && var->flags.b_sharp)
				conv->length += write(STD, "0X", 2);
			else if (var->flags.b_sharp)
				conv->length += write(STD, "0x", 2);
			field_width_with_zero(var, conv);
		}
		else if (conv->indic == 'X' && var->flags.b_sharp)
			conv->length += write(STD, "0X", 2);
		else
		{
			if ((var->flags.b_sharp && conv->indic == 'x') ||
					conv->indic == 'p')
				conv->length += write(STD, "0x", 2);
		}
		if (var->flags.b_minus)
			return (1);
	}
	return (0);
}

int		zero_d(t_var *var, t_conv *conv)
{
	if (conv->indic == 'd' || conv->indic == 'u' || conv->indic == 'i')
	{
		if (var->pr < var->fw && var->pr > conv->lenstr && !var->flags.b_minus)
		{
			if (conv->b_neg_num)
				var->fw -= 1;
			while (var->fw > var->pr)
			{
				conv->length += write(STD, " ", 1);
				var->fw--;
			}
			var->flags.b_field_width = false;
		}
		if (d_zero_pr_smaller_than_fw(var, conv))
			return (0);
		if (var->pr > conv->lenstr)
			field_width_with_zero(var, conv);
		d_zero_with_neg_num(var, conv);
		if (var->flags.b_minus)
			return (1);
	}
	return (0);
}

void	zero(t_var *var, t_conv *conv)
{
	zero_c(var, conv);
	if (zero_hexa(var, conv))
		return ;
	if (zero_d(var, conv))
		return ;
	if (var->flags.b_field_width && !var->flags.b_precision)
		while (var->fw > conv->lenstr)
		{
			conv->length += write(STD, "0", 1);
			var->fw--;
		}
	if (!var->flags.b_precision)
		var->flags.b_field_width = false;
}
