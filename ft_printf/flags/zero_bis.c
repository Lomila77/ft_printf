/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zero_bis.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 09:43:21 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 14:40:20 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flags.h"
#include "conv.h"
#include "libft.h"

void	field_width_with_zero(t_var *var, t_conv *conv)
{
	if (conv->indic == 'p' || conv->indic == 'x' || conv->indic == 'X')
	{
		while (var->fw > conv->lenstr && !var->flags.b_minus)
		{
			conv->length += write(STD, "0", 1);
			var->fw--;
		}
		while (var->pr > conv->lenstr && var->flags.b_minus)
		{
			conv->length += write(STD, "0", 1);
			var->pr--;
			var->fw--;
		}
	}
	if (conv->indic == 'd' || conv->indic == 'i' || conv->indic == 'u')
	{
		while (var->pr > conv->lenstr)
		{
			conv->length += write(STD, "0", 1);
			var->fw--;
			var->pr--;
		}
	}
}

int		d_zero_pr_smaller_than_fw(t_var *var, t_conv *conv)
{
	unsigned char *str;

	str = conv->str;
	if (var->pr > var->fw || var->pr > conv->lenstr ||
			(conv->b_neg_num && var->pr > conv->lenstr - 1))
	{
		if (conv->b_neg_num)
		{
			conv->length += write(STD, "-", 1);
			conv->str = (unsigned char *)ft_strdup((char *)&str[1]);
			free(str);
			conv->lenstr--;
			var->fw--;
		}
		field_width_with_zero(var, conv);
		if (var->pr > var->fw)
			var->flags.b_field_width = false;
		return (1);
	}
	return (0);
}

int		d_zero_with_neg_num(t_var *var, t_conv *conv)
{
	unsigned char *str;

	str = conv->str;
	if (conv->b_neg_num && (var->pr > var->fw || !var->flags.b_precision))
	{
		conv->length += write(STD, "-", 1);
		conv->str = (unsigned char *)ft_strdup((char *)&str[1]);
		free(str);
		conv->lenstr--;
		var->fw--;
		var->flags.b_zero = false;
		return (1);
	}
	return (0);
}
