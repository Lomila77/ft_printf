/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/14 17:25:40 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 16:49:41 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "flags.h"
#include "conv.h"
#include "libft.h"

int		print_something(const char *str, int *cursor, t_var *var)
{
	if ((*cursor) == 0 && str[(*cursor)] == '%')
		(*cursor)++;
	else if ((*cursor) > 0)
		var->nbr_o += write(STD, str, (*cursor)++);
	else
	{
		var->nbr_o += write(STD, str, ft_strlen(str));
		return (0);
	}
	return (1);
}

int		ft_printf(const char *str, ...)
{
	va_list	params;
	t_cur	cur;
	t_var	var;

	va_start(params, str);
	init_all(&var, &cur);
	while (str)
	{
		cur.old += cur.new;
		cur.new = ft_nstrchr(str, '%');
		if (!print_something(str, &cur.new, &var))
			break ;
		cur.new += init_flags(&str[cur.new], &var);
		if (str[cur.new] == '*')
			var.fw = va_arg(params, int);
		if (field_width(&str[cur.new], &var, &cur.new) < 0)
			break ;
		if (length_precision(&str[cur.new], &var, &cur.new, &params) < 0)
			break ;
		cur.new += length_mod(&str[cur.new], &var);
		cur.new += parsing_conv(&str[cur.new], &var, &params);
		str = &str[cur.new];
	}
	va_end(params);
	return (var.nbr_o);
}
