/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conv.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/14 17:36:05 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 16:49:24 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONV_H
# define CONV_H

# include "libft.h"
# include "struct.h"
# include <stdarg.h>
# include <stdbool.h>

# define STD 1

typedef struct			s_cur
{
	int					new;
	int					old;
}						t_cur;

typedef union			u_typecast
{
	int					i_di;
	unsigned int		ui_ux;
	char				hh_di;
	unsigned char		hh_ux;
	short				h_di;
	unsigned short		h_ux;
	long				l_di;
	unsigned long		l_ux;
	long long			ll_di;
	unsigned long long	ll_ux;

}						t_typecast;

typedef struct			s_lengthmod
{
	char				h;
	char				hh;
	char				l;
	char				ll;
}						t_lengthmod;

typedef struct			s_var
{
	int					fw;
	int					pr;
	int					nbr_o;
	t_lengthmod			lm;
	t_typecast			arg;
	void				*cast;
	t_flags				flags;
}						t_var;

typedef struct			s_conv
{
	unsigned char		indic;
	unsigned char		*str;
	unsigned long long	p;
	int					length;
	char				b_empty_s;
	char				b_neg_num;
	int					lenstr;
	int					(*f)(va_list *, t_var *, struct s_conv *);
}						t_conv;

int						c_conv(va_list *params, t_var *var, t_conv *conv);
int						s_conv(va_list *params, t_var *var, t_conv *conv);
int						p_conv(va_list *params, t_var *var, t_conv *conv);
int						d_conv(va_list *params, t_var *var, t_conv *conv);
int						xmaj_conv(va_list *params, t_var *var, t_conv *conv);
int						u_conv(va_list *params, t_var *var, t_conv *conv);
int						x_conv(va_list *params, t_var *var, t_conv *conv);
int						n_conv(va_list *params, t_var *var, t_conv *conv);
int						pourcent_conv(va_list *params, t_var *var,
									t_conv *conv);
void					display_hexa(unsigned long long conv_p, t_conv *conv);
void					display_hexa_xmaj(unsigned long long conv_p,
									t_conv *conv);
void					len_hexa(unsigned long long conv_p, t_conv *conv);
void					len_hexa_xmaj(unsigned long long conv_p, t_conv *conv);
void					init_struct(t_conv *content);
int						parsing_conv(const char *str, t_var *var,
									va_list *params);
void					cast_di(va_list *params, t_var *var, t_conv *conv);
void					cast_ux(va_list *params, t_var *var, t_conv *conv);
void					init_conv(t_conv *conv);
int						d_zero_with_neg_num(t_var *var, t_conv *conv);
int						d_zero_pr_smaller_than_fw(t_var *var, t_conv *conv);
void					field_width_with_zero(t_var *var, t_conv *conv);

#endif
