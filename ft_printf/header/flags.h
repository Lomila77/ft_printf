/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/14 16:51:50 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 16:51:06 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FLAGS_H
# define FLAGS_H

# include "libft.h"
# include "conv.h"
# include <stdarg.h>

int				ft_printf(const char *str,
							...) __attribute__((format(printf,1,2)));
int				init_flags(const char *str, t_var *var);
void			init_all(t_var *var, t_cur *cur);
int				field_width(const char *str, t_var *var, int *cursor);
void			width_is_start(t_flags *flags, int *field_w);
int				length_mod(const char *str, t_var *var);
void			minus(t_var *var, t_conv *conv);
void			minus_d(t_var *var, t_conv *conv);
void			minus_hexa(t_var *var, t_conv *conv);
void			minus_c(t_var *var, t_conv *conv);
void			plus(t_var *var, t_conv *conv);
void			plus_d(t_var *var, t_conv *conv);
int				length_precision(const char *str, t_var *var, int *cursor,
								va_list *params);
void			space(t_var *var, t_conv *conv);
void			space_hexa(t_var *var, t_conv *conv);
void			sharp(t_var *var, t_conv *conv);
void			zero(t_var *var, t_conv *conv);
void			zero_c(t_var *var, t_conv *conv);
int				zero_d(t_var *var, t_conv *conv);
int				zero_hexa(t_var *var, t_conv *conv);
int				call_flags(t_var *var, t_conv *conv);
void			write_fw(t_var *var, t_conv *conv);
#endif
