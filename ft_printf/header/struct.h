/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 10:29:35 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 14:20:34 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

typedef struct	s_flags
{
	char		b_sharp;
	char		b_minus;
	char		b_zero;
	char		b_plus;
	char		b_space;
	char		b_precision;
	char		b_field_width;
}				t_flags;
#endif
