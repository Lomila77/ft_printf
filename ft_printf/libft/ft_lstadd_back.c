/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_back.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/22 17:32:27 by gcolomer          #+#    #+#             */
/*   Updated: 2020/11/22 22:30:53 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static t_list	*ft_next(t_list *alst)
{
	if (alst->next != NULL)
		return (ft_next(alst->next));
	return (alst);
}

void			ft_lstadd_back(t_list **alst, t_list *new)
{
	if (*alst == NULL)
	{
		*alst = new;
		return ;
	}
	if (new == NULL)
		return ;
	(ft_next(*alst))->next = new;
}
