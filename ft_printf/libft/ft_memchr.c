/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/14 17:56:07 by gcolomer          #+#    #+#             */
/*   Updated: 2020/12/14 17:56:18 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char		ch;
	unsigned char		*str;
	size_t				i;

	ch = c;
	str = (unsigned char *)s;
	i = 0;
	while (i < n)
	{
		if (ch == str[i])
			return ((void *)&str[i]);
		i++;
	}
	return (NULL);
}
