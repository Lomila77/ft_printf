/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nstrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/05 10:08:54 by gcolomer          #+#    #+#             */
/*   Updated: 2021/02/05 10:09:20 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_nstrchr(const char *s, int c)
{
	char	ch;
	char	*str;
	size_t	i;

	ch = c;
	str = (char *)s;
	i = 0;
	while (str[i])
	{
		if (ch == str[i])
			return (i);
		i++;
	}
	if (str[i] == ch)
		return (i);
	return (0);
}
