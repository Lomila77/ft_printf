/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/19 09:04:50 by gcolomer          #+#    #+#             */
/*   Updated: 2020/11/20 21:26:34 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	i;
	size_t	j;
	char	*st1;
	char	*st2;
	char	*str;

	st1 = (char *)s1;
	st2 = (char *)s2;
	i = -1;
	j = -1;
	str = NULL;
	if (!(str = malloc(sizeof(char) * (ft_strlen(st1) + ft_strlen(st2) + 1))))
		return (str);
	str[ft_strlen(st1) + ft_strlen(st2)] = '\0';
	while (++i < ft_strlen(st1))
		str[i] = st1[i];
	while (++j < ft_strlen(st2))
	{
		str[i] = st2[j];
		i++;
	}
	return (str);
}
