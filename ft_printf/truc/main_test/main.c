#include <stdio.h>
#include "libft.h"
#include "conv.h"
#include "flags.h"

int	main(int argc, char **argv)
{
	int a;

	int b;

	a = ft_printf("Je suis une simple ligne\n");
	b = printf("Je suis une simple ligne\n");
	printf("a = %d\nb = %d\n", a, b);
	if (argv[1])
	{
		a = ft_printf(argv[1], argv[2]);
		printf("\n");
		b = printf(argv[1], argv[2]);
		printf("a = %d\n b = %d\n", a, b);
	}
	return (0);
}
