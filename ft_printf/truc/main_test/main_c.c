#include "libft.h"
#include "conv.h"
#include "flags.h"
#include <stdio.h>
int ft_printf(const char *str, ...); // Retirer et mettre dans un flags

int main(int argc, char **argv)
{
	int a = 0, b = 0;
	ft_printf("CONV_C :\n\nSimple :\nFT :\t%c\nPF :\t%c\n\n", 'G', 'G');
	    ft_printf("| ------------------------------- | --------------------- |  ---------------------- |\n");	
	a = ft_printf("| FT :\t'-' :\t\t($-c) %-c   |  ($-10c) %-10c   |  ($-10.15c) %-10.15c  |\n", 'G', 'G', 'G');	// - avec precision et largeur de champs
	b =    printf("| PF :\t'-' :\t\t($-c) %-c   |  ($-10c) %-10c   |  ($-10.15c) %-10.15c  |\n", 'G', 'G', 'G');  // - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- |  ---------------------- |\n");	
	a = ft_printf("| FT :\t'0' :\t\t($0c) %0c   |  ($010c) %010c   |  ($010.15c) %010.15c  |\n", 'G', 'G', 'G');    // 0 avec precision et largeur de champs
	b =    printf("| PF :\t'0' :\t\t($0c) %0c   |  ($010c) %010c   |  ($010.15c) %010.15c  |\n", 'G', 'G', 'G');  // - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- |  ---------------------- |\n");
	a = ft_printf("| FT :\t' ' :\t\t($ c) %-c   |  ($ 10c) % 10c   |  ($ 10.15c) % 10.15c  |\n", 'G', 'G', 'G');    // ' ' avec precision et largeur de champs
	b =    printf("| PF :\t' ' :\t\t($ c) % c   |  ($ 10c) % 10c   |  ($ 10.15c) % 10.15c  |\n", 'G', 'G', 'G');  // - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------  | --------------------- |  ---------------------- |\n");
	a = ft_printf("| FT :\t'+' :\t\t($+c) %+c   |  ($+10c) %+10c   |  ($+10.15c) %+10.15c  |\n", 'G', 'G', 'G');    // + avec precision et largeur de champs
	b =    printf("| PF :\t'+' :\t\t($+c) %+c   |  ($+10c) %+10c   |  ($+10.15c) %+10.15c  |\n", 'G', 'G', 'G');  // - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'#' :\t\t($#c) %#c   |  ($#10c) %#10c   |  ($#10.15c) %#10.15c  |\n", 'G', 'G', 'G');    // # avec precision et largeur de champs
	b =    printf("| PF :\t'#' :\t\t($#c) %#c   |  ($#10c) %#10c   |  ($#10.15c) %#10.15c  |\n", 'G', 'G', 'G');  // - avec precision et largeur de champs	
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n\n");	
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'0' + '-' :\t($0-c) %0-c  |  ($0-10c) %0-10c  |  ($0-10.15c) %0-10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t'0' + '-' :\t($0-c) %0-c  |  ($0-10c) %0-10c  |  ($0-10.15c) %0-10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'-' + '0' :\t($-0c) %-0c  |  ($-010c) %-010c  |  ($-010.15c) %-010.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t'-' + '0' :\t($-0c) %-0c  |  ($-010c) %-010c  |  ($-010.15c) %-010.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");	
	a = ft_printf("| FT :\t' ' + '0' :\t($ 0c) % 0c  |  ($ 010c) % 010c  |  ($ 010.15c) % 010.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t' ' + '0' :\t($ 0c) % 0c  |  ($ 010c) % 010c  |  ($ 010.15c) % 010.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'0' + ' ' :\t($0 c) %0 c  |  ($0 10c) %0 10c  |  ($0 10.15c) %0 10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t'0' + ' ' :\t($0 c) %0 c  |  ($0 10c) %0 10c  |  ($0 10.15c) %0 10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'-' + ' ' :\t($- c) %- c  |  ($- 10c) %- 10c  |  ($- 10.15c) %- 10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t'-' + ' ' :\t($- c) %- c  |  ($- 10c) %- 10c  |  ($- 10.15c) %- 10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'#' + ' ' :\t($# c) %# c  |  ($# 10c) %# 10c  |  ($# 10.15c) %# 10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t'#' + ' ' :\t($- c) %# c  |  ($# 10c) %# 10c  |  ($# 10.15c) %# 10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'-' + '#' :\t($- c) %- c  |  ($- 10c) %- 10c  |  ($- 10.15c) %- 10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t'-' + '#' :\t($- c) %- c  |  ($- 10c) %- 10c  |  ($- 10.15c) %- 10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'0' + '#' :\t($0#c) %0#c  |  ($0#10c) %0#10c  |  ($0#10.15c) %0#10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t'0' + '#' :\t($0#c) %0#c  |  ($0#10c) %0#10c  |  ($0#10.15c) %0#10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'+' + '#' :\t($+#c) %+#c  |  ($+#10c) %+#10c  |  ($+#10.15c) %+#10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t'+' + '#' :\t($+#c) %+#c  |  ($+#10c) %+#10c  |  ($+#10.15c) %+#10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'+' + ' ' :\t($+ c) %+ c  |  ($+ 10c) %+ 10c  |  ($+ 10.15c) %+ 10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t'+' + ' ' :\t($+ c) %+ c  |  ($+ 10c) %+ 10c  |  ($+ 10.15c) %+ 10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'0' + '+' :\t($0+c) %0+c  |  ($+010c) %0+10c  |  ($0+10.15c) %0+10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t'0' + '+' :\t($0+c) %0+c  |  ($0+10c) %0+10c  |  ($0+10.15c) %0+10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- |  -------------------- | ----------------------- |\n");
	a = ft_printf("| FT :\t'-' + '+' :\t($-+c) %-+c  |  ($-+10c) %-+10c  |  ($-+10.15c) %-+10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	b =    printf("| PF :\t'-' + '+' :\t($-+c) %-+c  |  ($-+10c) %-+10c  |  ($-+10.15c) %-+10.15c |\n", 'G', 'G', 'G');    // 0 et - avec precision et largeur de champs
	       printf("| FT = %d | PF = %d ------------- | --------------------- | ----------------------- |\n", a, b);
	    ft_printf("| ------------------------------- | --------------------- | ----------------------- |\n");
		ft_printf("EMPTY STRING :\n");
		a = ft_printf("FT : %c\n", '\0');
		b = printf("PF : %c\n", '\0');
		printf("FT = %d | PF = %d\n", a, b);
	if (argv[1])
	{
		ft_printf("STDIN :\n");
		a = ft_printf(argv[1], argv[2][0]);
		printf("\nFT = %d\n", a);
		ft_printf("\n");
		printf("STDIN PF :\n");
		b = printf(argv[1], argv[2][0]);
		printf("\nPF = %d", b);
		ft_printf("\n");
	}
	return (0);
}
