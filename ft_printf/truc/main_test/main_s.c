#include "libft.h"
#include "conv.h"
#include "flags.h"
#include <stdio.h>
#include <string.h>
int ft_printf(const char *str, ...); // Retirer et mettre dans un flags

int main(int argc, char **argv)
{
	int a = 0, b = 0;
	char *s = "CONV_S";
	ft_printf("CONV_S :\n\nSimple :\nFT :\t%s\nPF :\t%s\n\n", s, s);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");	
	a = ft_printf("| FT :\t'-' :\t\t($-s) %-s   |  ($-10s) %-10s   |  ($-10.2s) %-10.2s  |\n", s, s, s);	// - aves presision et largeur de shamps
	b =    printf("| PF :\t'-' :\t\t($-s) %-s   |  ($-10s) %-10s   |  ($-10.2s) %-10.2s  |\n", s, s, s);  // - aves presision et largeur de shamps
		   printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");	
	a = ft_printf("| FT :\t'0' :\t\t($0s) %0s   |  ($010s) %010s   |  ($010.2s) %010.2s  |\n", s, s, s);    // 0 aves presision et largeur de shamps
	b =    printf("| PF :\t'0' :\t\t($0s) %0s   |  ($010s) %010s   |  ($010.2s) %010.2s  |\n", s, s, s);  // - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t' ' :\t\t($ s) %-s   |  ($ 10s) % 10s   |  ($ 10.2s) % 10.2s  |\n", s, s, s);    // ' ' aves presision et largeur de shamps
	b =    printf("| PF :\t' ' :\t\t($ s) % s   |  ($ 10s) % 10s   |  ($ 10.2s) % 10.2s  |\n", s, s, s);  // - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t'+' :\t\t($+s) %+s   |  ($+10s) %+10s   |  ($+10.02s) %+10.2s |\n", s, s, s);    // + aves presision et largeur de shamps
	b =    printf("| PF :\t'+' :\t\t($+s) %+s   |  ($+10s) %+10s   |  ($+10.02s) %+10.2s |\n", s, s, s);  // - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	    ft_printf("| FT :\t'#' :\t\t($#s) %#s   |  ($#10s) %#10s   |  ($#10.02s) %#10.2s |\n", s, s, s);    // # aves presision et largeur de shamps
	 a =   printf("| PF :\t'#' :\t\t($#s) %#s   |  ($#10s) %#10s   |  ($#10.02s) %#10.2s |\n", s, s, s);  // - aves presision et largeur de shamps	
	 b =   printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n\n");	
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t'0' + '-' :\t($0-s) %0-s  |  ($0-10s) %0-10s  |  ($0-10.2s) %0-10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t'0' + '-' :\t($0-s) %0-s  |  ($0-10s) %0-10s  |  ($0-10.2s) %0-10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t'-' + '0' :\t($-0s) %-0s  |  ($-010s) %-010s  |  ($-010.2s) %-010.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t'-' + '0' :\t($-0s) %-0s  |  ($-010s) %-010s  |  ($-010.2s) %-010.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");	
	a = ft_printf("| FT :\t' ' + '0' :\t($ 0s) % 0s  |  ($ 010s) % 010s  |  ($ 010.2s) % 010.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t' ' + '0' :\t($ 0s) % 0s  |  ($ 010s) % 010s  |  ($ 010.2s) % 010.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t'0' + ' ' :\t($0 s) %0 s  |  ($0 10s) %0 10s  |  ($0 10.2s) %0 10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t'0' + ' ' :\t($0 s) %0 s  |  ($0 10s) %0 10s  |  ($0 10.2s) %0 10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t'-' + ' ' :\t($- s) %- s  |  ($- 10s) %- 10s  |  ($- 10.2s) %- 10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t'-' + ' ' :\t($- s) %- s  |  ($- 10s) %- 10s  |  ($- 10.2s) %- 10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t'#' + ' ' :\t($# s) %# s  |  ($# 10s) %# 10s  |  ($# 10.2s) %# 10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t'#' + ' ' :\t($# s) %# s  |  ($# 10s) %# 10s  |  ($# 10.2s) %# 10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t'-' + '#' :\t($- s) %- s  |  ($- 10s) %- 10s  |  ($- 10.2s) %- 10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t'-' + '#' :\t($- s) %- s  |  ($- 10s) %- 10s  |  ($- 10.2s) %- 10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t'0' + '#' :\t($0#s) %0#s  |  ($0#10s) %0#10s  |  ($0#10.2s) %0#10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t'0' + '#' :\t($0#s) %0#s  |  ($0#10s) %0#10s  |  ($0#10.2s) %0#10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t'+' + '#' :\t($+#s) %+#s  |  ($+#10s) %+#10s  |  ($+#10.2s) %+#10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t'+' + '#' :\t($+#s) %+#s  |  ($+#10s) %+#10s  |  ($+#10.2s) %+#10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t'+' + ' ' :\t($+ s) %+ s  |  ($+ 10s) %+ 10s  |  ($+ 10.2s) %+ 10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t'+' + ' ' :\t($+ s) %+ s  |  ($+ 10s) %+ 10s  |  ($+ 10.2s) %+ 10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\t'0' + '+' :\t($0+s) %0+s  |  ($+010s) %0+10s  |  ($0+10.2s) %0+10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t'0' + '+' :\t($0+s) %0+s  |  ($0+10s) %0+10s  |  ($0+10.2s) %0+10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- | ---------------------- |\n");
	a = ft_printf("| FT :\t'-' + '+' :\t($-+s) %-+s  |  ($-+10s) %-+10s  |  ($-+10.2s) %-+10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	b =    printf("| PF :\t'-' + '+' :\t($-+s) %-+s  |  ($-+10s) %-+10s  |  ($-+10.2s) %-+10.2s |\n", s, s, s);    // 0 et - aves presision et largeur de shamps
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	    ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
	a = ft_printf("| FT :\tX : ($s$s$s$s$s) %s%s%s%s%s ------------------------------- |\n", s, s, s, s, s);
	b =    printf("| PF :\tX : ($s$s$s$s$s) %s%s%s%s%s ------------------------------- |\n", s, s, s, s, s);
	       printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
		ft_printf("| ------------------------------------ |  -------------------- |  --------------------- |\n");
		ft_printf("| EMPTY STRING : --------------------- | --------------------- | ---------------------- |\n");
	a = ft_printf("| FT : %s ------------------------ | --------------------- | ---------------------- |\n", NULL);
	   b = printf("| PF : %s ------------------------ | --------------------- | ---------------------- |\n", NULL);
		   printf("| FT = %d | PF = %d ------------------ | --------------------- | ---------------------- |\n", a, b);
	if (argv[1])
	{
		ft_printf("ARG :\nFT :\n");
		a = ft_printf(argv[1], argv[2]);
		printf("\nRetour = %d\n", a);
		ft_printf("\n");
		printf("ARG\nPF :\n");
		b = printf(argv[1], argv[2]);
		printf("\nRetour = %d\n", b);
		printf("\n");
	}
	a = ft_printf("\n%3.7s%3.3s\n", "hello", "world");
	b = printf("%3.7s%3.3s\n", "hello", "world");
	printf ("FT : %d | PF : %d\n\n", a, b);

		ft_printf("\n\nTEST %%i\n");
	a = ft_printf("FT : ($-*.*s) %-*.*s|\n", -7, -3, "Bonjour");
	b =    printf("PF : ($-*.*s) %-*.*s|\n", -7, -3, "Bonjour");
	printf("FT : %d | PF : %d\n", a, b);
	a = ft_printf("FT : ($.*s) %*s|\n", -3, "Bonjour");
	b =    printf("PF : ($.*s) %*s|\n", -3, "Bonjour");
	printf("FT : %d | PF : %d\n", a, b);
	return (0);
}
