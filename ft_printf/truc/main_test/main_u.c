#include "libft.h"
#include "conv.h"
#include "flags.h"
#include <stdio.h>
int ft_printf(const char *str, ...); // Retirer et mettre uans un flags

int main(int argc, char **argv)
{
	int a = 0, b = 0;
	int conv = 42;
	ft_printf("CONV_p :\n\nSimple :\nFT :\t%u\n",  conv);
       printf("PF :\t%u\n\n",  conv);
	    ft_printf("| ---------------------------------- | --------------------- |  ----------------------------- |\n");	
	a = ft_printf("| FT :\t'-' :\t\t($-u) %-u     |  ($-10u) %-10u   |  ($-10.15u) %-10.15u    |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'-' :\t\t($-u) %-u     |  ($-10u) %-10u   |  ($-10.15u) %-10.15u    |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");	
	a = ft_printf("| FT :\t'0' :\t\t($0u) %0u     |  ($010u) %010u   |  ($010.15u) %010.15u    |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'0' :\t\t($0u) %0u     |  ($010u) %010u   |  ($010.15u) %010.15u    |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t' ' :\t\t($ u) % u    |  ($ 10u) % 10u   |  ($ 10.15u) % 10.15u   |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t' ' :\t\t($ u) % u    |  ($ 10u) % 10u   |  ($ 10.15u) % 10.15u   |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------  | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'+' :\t\t($+u) %+u    |  ($+10u) %+10u   |  ($+10.15u) %+10.15u   |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'+' :\t\t($+u) %+u    |  ($+10u) %+10u   |  ($+10.15u) %+10.15u   |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'#' :\t\t($#u) %#u     |  ($#10u) %#10u   |  ($#10.15u) %#10.15u    |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'#' :\t\t($#u) %#u     |  ($#10u) %#10u   |  ($#10.15u) %#10.15u    |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");	
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'0' + '-' :\t($0-u) %0-u    |  ($0-10u) %0-10u  |  ($0-10.15u) %0-10.15u   |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'0' + '-' :\t($0-u) %0-u    |  ($0-10u) %0-10u  |  ($0-10.15u) %0-10.15u   |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'-' + '0' :\t($-0u) %-0u    |  ($-010u) %-010u  |  ($-010.15u) %-010.15u   |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'-' + '0' :\t($-0u) %-0u    |  ($-010u) %-010u  |  ($-010.15u) %-010.15u   |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");	
	a = ft_printf("| FT :\t' ' + '0' :\t($ 0u) % 0u   |  ($ 010u) % 010u  |  ($ 010.15u) % 010.15u  |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t' ' + '0' :\t($ 0u) % 0u   |  ($ 010u) % 010u  |  ($ 010.15u) % 010.15u  |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'0' + ' ' :\t($0 u) %0 u   |  ($0 10u) %0 10u  |  ($0 10.15u) %0 10.15u  |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'0' + ' ' :\t($0 u) %0 u   |  ($0 10u) %0 10u  |  ($0 10.15u) %0 10.15u  |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'-' + ' ' :\t($- u) %- u   |  ($- 10u) %- 10u  |  ($- 10.15u) %- 10.15u  |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'-' + ' ' :\t($- u) %- u   |  ($- 10u) %- 10u  |  ($- 10.15u) %- 10.15u  |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'#' + ' ' :\t($# u) %# u   |  ($# 10u) %# 10u  |  ($# 10.15u) %# 10.15u  |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'#' + ' ' :\t($- u) %# u   |  ($# 10u) %# 10u  |  ($# 10.15u) %# 10.15u  |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'-' + '#' :\t($-#u) %-#u    |  ($-#10u) %-#10u  |  ($-#10.15u) %-#10.15u   |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'-' + '#' :\t($-#u) %-#u    |  ($-#10u) %-#10u  |  ($-#10.15u) %-#10.15u   |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'0' + '#' :\t($0#u) %0#u    |  ($0#10u) %0#10u  |  ($0#10.15u) %0#10.15u   |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'0' + '#' :\t($0#u) %0#u    |  ($0#10u) %0#10u  |  ($0#10.15u) %0#10.15u   |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'+' + '#' :\t($+#u) %+#u   |  ($+#10u) %+#10u  |  ($+#10.15u) %+#10.15u  |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'+' + '#' :\t($+#u) %+#u   |  ($+#10u) %+#10u  |  ($+#10.15u) %+#10.15u  |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'+' + ' ' :\t($+ u) %+ u   |  ($+ 10u) %+ 10u  |  ($+ 10.15u) %+ 10.15u  |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'+' + ' ' :\t($+ u) %+ u   |  ($+ 10u) %+ 10u  |  ($+ 10.15u) %+ 10.15u  |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'0' + '+' :\t($0+u) %0+u   |  ($+010u) %0+10u  |  ($0+10.15u) %0+10.15u  |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'0' + '+' :\t($0+u) %0+u   |  ($0+10u) %0+10u  |  ($0+10.15u) %0+10.15u  |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- |  -------------------- | ------------------------------ |\n");
	a = ft_printf("| FT :\t'-' + '+' :\t($-+u) %-+u   |  ($-+10u) %-+10u  |  ($-+10.15u) %-+10.15u  |\n",  conv,  conv,  conv);
	b =    printf("| PF :\t'-' + '+' :\t($-+u) %-+u   |  ($-+10u) %-+10u  |  ($-+10.15u) %-+10.15u  |\n",  conv,  conv,  conv);
	       printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
	    ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
		ft_printf("| EMPTY STRING : ------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT : (0)  %u ---------------------- | (NULL)   %u ---------- | ------------------------------ |\n", 0, NULL);
	   b = printf("| PF : (0)  %u ---------------------- | (NULL)   %u ---------- | ------------------------------ |\n", 0, NULL);
		   printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
		ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
		ft_printf("| WIuTH START ---------------------- | --------------------- | ------------------------------ |\n");
	a = ft_printf("| FT : ($**.*u) %**.*u ----------------- |  ($*.**u) %*.**u -------- | ($****.****u) %****.****u - |\n", 10, 15, conv, 10, 15, conv, 10, 15, conv);
		ft_printf("| Boucle infinie sur printf(mac) --- | --------------------- | ------------------------------ |\n");
//	b =    printf("| PF : ($**.*p) %**.*p ---------- |  ($*.**p) %*.**p ---- | ($****.****p) %****.****p ---- |\n\n", 10, 15, conv, 10, 15, conv, 10, 15, conv);
		   printf("| FT = %u | PF = %u ---------------- | --------------------- | ------------------------------ |\n", a, b);
		ft_printf("| ---------------------------------- | --------------------- | ------------------------------ |\n");
	if (argv[1])
	{
		ft_printf("STuIN FT : ");
		a = ft_printf(argv[1],  atoi(argv[2]));
		printf("\n\nFT = %u\n", a);
		ft_printf("\n");
		printf("STuIN PF : ");
		b = printf(argv[1],  atoi(argv[2]));
		printf("\nPF = %u", b);
		ft_printf("\n");
	}
	return (0);
}
